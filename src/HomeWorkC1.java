import java.util.Scanner;

public class HomeWorkC1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter the number of rows: ");
        int numRows = Integer.parseInt(scan.nextLine());

        String[] arrayOfRows = new String[numRows];

        for (int i = 0; i < numRows; i++) {
            System.out.println(String.format("Enter the row number %d: ", i + 1));
            String row = scan.nextLine();
            arrayOfRows[i] = row;
        }

        int minLength = arrayOfRows[0].length();
        String minLengthRow = arrayOfRows[0];

        for (int i = 0; i < numRows; i++) {
            if (minLength > arrayOfRows[i].length()) {
                minLength = arrayOfRows[i].length();
                minLengthRow = arrayOfRows[i];
            }
        }

        System.out.println("The minimum row is: " + minLengthRow);
        System.out.println("Num symbols is: " + minLength);
    }
}

