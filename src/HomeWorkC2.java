import java.util.Scanner;

public class HomeWorkC2 {
    public static void main(String[] args) {

        int numRow;

        System.out.println("Введите количество строк");
        Scanner scanNumRow = new Scanner(System.in);

        numRow = scanNumRow.nextInt();

        String[] arrayRows = new String[numRow];
        Scanner scanStrRow = new Scanner(System.in);

        for (int i = 0; i < numRow; i++) {
            System.out.println("Введите строку №" + (i + 1));
            arrayRows[i] = scanStrRow.nextLine();
        }
        float average = 0f;

        for (String item : arrayRows) {
            average += (float) item.length();
        }

        average /= numRow;

        System.out.println("Средняя длина строки = (" + average + ")");

        for (int i = 0; i < numRow; i++) {
            if (arrayRows[i].length() < average) {
                System.out.println("Строка меньшая чем средняя длина: ");
                System.out.println(arrayRows[i] + " ee длина = " + arrayRows[i].length());
            }
        }

    }
}
