import java.util.Scanner;

public class HomeWorkC3 {
    public static void main(String[] args) {
        String row;
        Scanner in = new Scanner(System.in);
        System.out.println("Введите кол-во строк: ");
        int numRows = Integer.parseInt(in.nextLine());
        String[] arrayRows = new String[numRows];

        for (int i = 0; i < numRows; i++) {
            System.out.println(String.format("Type row %d:", i + 1));
            row = in.nextLine().toLowerCase();
            arrayRows[i] = row;
        }

        String findWord = "дерево";

        for (int i = 0; i < numRows; i++) {
            if (arrayRows[i].contains(findWord)) {
                System.out.println(String.format("Нашёл слово %s в строке %d ",findWord, i + 1));
            }

        }
    }
}
