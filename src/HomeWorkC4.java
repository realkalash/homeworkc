import java.util.Scanner;

public class HomeWorkC4 {
    public static void main(String[] args) {
        String row;
        String rowForTest;
        Scanner in = new Scanner(System.in);
        System.out.println("Введите кол-во строк: ");
        int numRows = Integer.parseInt(in.nextLine());
        String[] arrayRows = new String[numRows];
        for (int i = 0; i < numRows; i++) {
            System.out.println(String.format("Type row %d:", i + 1));
            row = in.nextLine().toLowerCase();

            boolean test = row.matches("^(?i:[bcdfghjklmnpqrstvwxyz]).*");

            if (test && row.length() == 5) {

                row.replaceAll("^$", "");
            } else
                arrayRows[i] = row;
        }
        for (int i = 0; i < numRows; i++) {
            if (arrayRows[i] == null) {
                System.out.println(String.format("Была удалена строка номер: %d", i + 1));
            }
        }
    }
}